package org.learn.module.five.util;

import java.util.function.Supplier;

public final class ConsoleUtil {
    private ConsoleUtil() {
        throw new UnsupportedOperationException();
    }
    public static <T> T readValue(Supplier<T> supplier, Runnable reset, String message) {
        if (supplier == null) {
            throw new RuntimeException("Supplier is not found.");
        }
        if (reset == null) {
            throw new RuntimeException("Reset is not found");
        }
        T result = null;
        do {
            reset.run();
            System.out.print(message);
            try {
                result = supplier.get();
            } catch (Exception exception) {
                System.out.println("Something wrong: " + exception.getMessage());
            }
        } while (!ConsoleUtil.successCondition(result));
        return result;
    }

    public static <T> boolean successCondition(T input) {
        if (input == null) {
            return false;
        }
        if (input instanceof String){
            return !((String)input).isBlank();
        }
        return true;
    }

}
