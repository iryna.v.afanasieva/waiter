package org.learn.module.five.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;

import javax.jms.JMSException;
import javax.jms.Session;

public class Connection implements AutoCloseable {
    private final javax.jms.Connection connection;
    public Connection() throws JMSException {
        ActiveMQConnectionFactory connectionFactory =
            new ActiveMQConnectionFactory("admin", "admin", "tcp://localhost:61616");
        connection = connectionFactory.createConnection();
    }

    @Override
    public void close() throws JMSException {
        if (connection == null) {
            throw new RuntimeException("Connection is not found");
        }
        connection.close();
    }

    public Session createSession() throws JMSException {
        if (connection == null) {
            throw new RuntimeException("Connection is not found");
        }
        connection.start();
        return connection.createSession(true, ActiveMQSession.SESSION_TRANSACTED);
    }

}
