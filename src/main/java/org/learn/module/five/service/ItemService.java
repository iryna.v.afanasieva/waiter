package org.learn.module.five.service;

import org.learn.module.five.model.Item;
import org.learn.module.five.model.ItemType;
import org.learn.module.five.util.ConsoleUtil;

import java.util.Scanner;

public class ItemService {

    public Item createItem(Scanner scanner) {
        if (scanner == null) {
            throw new RuntimeException("Scanner is not found");
        }
        var title = ConsoleUtil.readValue(scanner::nextLine, scanner::reset, "Input title: ");
        var itemType = readItemType(scanner);
        float volume = 0;
        int qty = 0;
        if (ItemType.LIQUID.equals(itemType)) {
            volume = ConsoleUtil.readValue(scanner::nextFloat, scanner::reset, "Input volume: ");
        } else {
            qty = ConsoleUtil.readValue(scanner::nextInt, scanner::reset, "Input qty: ");
        }

        return new Item(title, itemType, volume, qty);
    }

    private ItemType readItemType(Scanner scanner) {
        var itemTypeValue = ConsoleUtil.readValue(scanner::nextLine, scanner::reset, "Choose type Liquid/L/l or Solid/S/s (default): ");
        switch (itemTypeValue) {
            case "L":
            case "l":
            case "Liquid": return ItemType.LIQUID;
            default: return ItemType.SOLID;
        }
    }

}
