package org.learn.module.five.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class Order {
    private final List<Item> items;

    public Order() {
        items = new ArrayList<>();
    }

    public List<Item> getItems() {
        return items;
    }

    public int addItem(Item item) {
        if (items == null) {
            throw new RuntimeException("Items not found.");
        }
        if (item != null) {
            items.add(item);
        }
        return items.size();
    }

    public int getTotalQty() {
        if (items == null) {
            return 0;
        }
        return items.stream()
            .filter(Objects::nonNull)
            .filter(item -> ItemType.SOLID.equals(item.getType()))
            .map(Item::getQty)
            .filter(Objects::nonNull)
            .reduce(Integer::sum)
            .orElse(0);
    }

    public float getTotalVolume() {
        if (items == null) {
            return 0;
        }
        return items.stream()
            .filter(Objects::nonNull)
            .filter(item -> ItemType.LIQUID.equals(item.getType()))
            .map(Item::getVolume)
            .filter(Objects::nonNull)
            .reduce(Float::sum)
            .orElse(0.0f);
    }

    public int getTotal() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }
}
