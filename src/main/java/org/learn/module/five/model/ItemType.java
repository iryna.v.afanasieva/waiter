package org.learn.module.five.model;

public enum ItemType {
    LIQUID, SOLID
}
