package org.learn.module.five.model;

public final class Item {
    private final String title;
    private final ItemType type;
    private final float volume;
    private final int qty;

    public Item(final String title, final ItemType type, final float volume, final int qty) {
        this.title = title;
        this.type = type;
        this.volume = volume;
        this.qty = qty;
    }

    public ItemType getType() {
        return type;
    }

    public float getVolume() {
        return volume;
    }

    public int getQty() {
        return qty;
    }

    public String getTitle() {
        return title;
    }
}
