package org.learn.module.five;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.learn.module.five.configuration.Connection;
import org.learn.module.five.model.Order;
import org.learn.module.five.service.ItemService;
import org.learn.module.five.util.ConsoleUtil;

import java.util.Scanner;
import javax.jms.JMSException;

public class SimpleConsoleApp {
    private static final ObjectMapper mapper = new ObjectMapper();

    public static void main(String[] args) throws JMSException, JsonProcessingException {
        try(
            Scanner scanner = new Scanner(System.in);
            Connection connection = new Connection();
            Producer producer = new Producer(connection, "order-queue")
        ) {
            Order order = new Order();
            ItemService service = new ItemService();
            String command = "";
            do {

                order.addItem(service.createItem(scanner));

                command = ConsoleUtil.readValue(scanner::nextLine, scanner::reset, "To send order press S/s: ");

                if ("S".equalsIgnoreCase(command)) {
                    final var message = convertOrder(order);
                    producer.send(message);
                    order = new Order();
                }

                command = ConsoleUtil.readValue(scanner::nextLine, scanner::reset, "To exit press E/e: ");

            } while (!"E".equalsIgnoreCase(command));
        }
    }

    private static String convertOrder(Order order) throws JsonProcessingException {
        return mapper.writeValueAsString(order);
    }

}
